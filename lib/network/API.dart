// ignore_for_file: unrelated_type_equality_checks, curly_braces_in_flow_control_structures, prefer_typing_uninitialized_variables

import 'dart:io';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';

class API {
/////// ===================  DEV Block   =================ō===============================

  static String baseURL =
      'https://itriangletechnolabs.com/projects/cliq_app_old_design/api/';

  static String baseURL1 = 'https://apistaging.checkn.com/';

  // =================    V5 version Api Call  ==============================================
  static String sendotp = baseURL + 'send_otp';
  static String getWorkouts = baseURL1 + 'common/app/settings';
  static String homesetting = baseURL1 + 'user/home_settings/160';
  static String DeleteAPI = 'https://fhudi.com/fitness/api/user/user_sleep/50';

  static int successStatusCode = 200;
  static int badRequest = 400;
  static int notFound = 404;

  static Dio dio = Dio();

  static Future init() async {
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    dio.options.followRedirects = false;
    dio.options.validateStatus = (status) {
      return status! < 500;
    };
    dio.options.connectTimeout = 120000; //5s
    dio.options.receiveTimeout = 200000;
    return;
  }

  static Future<void> apiCalling(
      {String? url,
      Map? params,
      String? method,
      Function? onError,
      Function? RESPONSE}) async {
    print('API URL ==> $url');
    print('Params ==> ${params}');
    // dio.options.headers['apikey'] =
    //     '0499199137423424fds952747394.u1lm0rfqsdft0e2';
    dio.options.headers['accept'] = 'application/x-www-form-urlencoded';
    dio.options.headers['Content-Type'] = 'application/x-www-form-urlencoded';
    dio.options.headers['Token'] =
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiMjMifQ.PnhAY0ntVqdTBLjORP5Fp50yyHaOgE7BCx_l1Zbvg2Q';

    try {
      var response;
      if (method == "POST") {
        response = await dio.post(url ?? '', data: params);
      } else if (method == "GET") {
        response = await dio.get(url ?? '');
      } else if (method == "PUT") {
        response = await dio.put(url ?? '', data: params);
      } else {
        response = await dio.delete(url ?? '');
      }
      printAPIResponseAndResponseCode(response);
      RESPONSE!(response.data);
    } catch (e) {
      print('e:::,$e');
      errorHandaling(e, onError!);
    }
    return;
  }

  static void errorHandaling(dynamic e, Function onError) {
    DioError dioError;
    String statuscode = '';
    try {
      dioError = e;
      statuscode = dioError.response!.statusCode.toString();

      print("CODE==========>" + dioError.response!.statusCode.toString());
      print("ERROR_TYPE==========>" + dioError.response!.data);
      print("ERROR==========>" + dioError.error);
      print("ERROR==========>" + dioError.response.toString());
    } catch (e) {
    } finally {
      // ignore: unnecessary_null_comparison
      if (DioError != null) {
        if (DioErrorType == DioErrorType.connectTimeout ||
            DioErrorType == DioErrorType.receiveTimeout ||
            DioErrorType == DioErrorType.sendTimeout) {
          onError("Connection TimeOut", true);
        } else {
          print(e.toString());
          onError('Server Error2 ' + statuscode, true);
        }
      } else
        print(e.toString());
      onError('Server Error1 ' + statuscode, true);
    }
  }

  static bool isValidResponseV2(Response response) {
    if (response.statusCode! > 199 && response.statusCode! < 499) {
      try {
        return response.data['status'] ?? false;
      } catch (e) {
        return false;
      }
    } else {
      return false;
    }
  }

  static void printAPIResponseAndResponseCode(Response<dynamic> response,
      [bool showData = true]) {
    print('response ==> ${response}');
    print('response data ==> ${response.data}');
    if (showData) print('response code ==> ${response.statusCode}');
  }
}
