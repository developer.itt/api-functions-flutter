import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoadingIndicator {
  static Widget isLoding() {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          color: (Colors.black).withOpacity(0.4),
          child: Center(
            child: Container(
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
              width: 90,
              height: 90,
              child: const SpinKitCircle(
                color: Colors.red,
                size: 65,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
