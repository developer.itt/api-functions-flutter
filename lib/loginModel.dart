// ignore_for_file: non_constant_identifier_names

class LoginModel {
  bool? status;
  String? success_msg;
  String? error_msg;
  String? phone;
  String? country_code;
  int? OTP;
  String? user_id;

  LoginModel({
    this.status,
    this.success_msg,
    this.error_msg,
    this.phone,
    this.country_code,
    this.OTP,
    this.user_id,
  });

  factory LoginModel.fromjson(Map<String, dynamic> json) {
    return LoginModel(
      status: json['status'],
      success_msg: json['success_msg'],
      error_msg: json['error_msg'],
      phone: json['phone'],
      country_code: json['country_code'],
      OTP: json['OTP'],
      user_id: json['user_id'],
    );
  }
}
