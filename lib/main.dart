import 'package:api_calling/loginModel.dart';
import 'package:api_calling/util/loadingIndicator.dart';
import 'package:api_calling/util/util.dart';
import 'package:flutter/material.dart';

import 'network/API.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'API Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'API Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Stack(
        children: [
          Center(
            child: InkWell(
              onTap: () {
                // API_POST('(777)666-4444');
                // API_GET();
                // API_PUT();
                API_Delete();
              },
              child: Container(
                color: Colors.red,
                height: 50,
                width: 200,
                child: const Center(child: Text('API CALL')),
              ),
            ),
          ),
          Visibility(visible: isLoading, child: LoadingIndicator.isLoding())
        ],
      ),
    );
  }

  // Function's

  void onLoaderDismiss() {
    setState(() {
      isLoading = false;
    });
  }

  void onLoaderShow() {
    setState(() {
      isLoading = true;
    });
  }

  API_POST(String phone) async {
    onLoaderShow();
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        Map _params = {
          'country_code': '+1',
          'phone': phone,
          'secretkey': 'AISOdQyAH3m2IIzaSSLzpBz1iyOsKyYCktzstLN',
        };
        await API.apiCalling(
            url: API.sendotp,
            params: _params,
            method: "POST",
            onError: (String errorMessage, bool isError) {
              Utility.alert(errorMessage);
              onLoaderDismiss();
            },
            RESPONSE: (Response) async {
              var response = LoginModel.fromjson(Response);
              if (response.status == true) {
                print(response.OTP);
              } else {
                Utility.alert(response.error_msg ?? '');
              }
              onLoaderDismiss();
            });
      } else {
        Utility.alert("No Internet connection.");
        onLoaderDismiss();
      }
    });
  }

  API_GET() async {
    onLoaderShow();
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        Map _params = {};
        await API.apiCalling(
            url: API.getWorkouts,
            params: _params,
            method: "GET",
            onError: (String errorMessage, bool isError) {
              Utility.alert(errorMessage);
              onLoaderDismiss();
            },
            RESPONSE: (Response) async {
              print("Response");
              print(Response);
              onLoaderDismiss();
            });
      } else {
        Utility.alert("No Internet connection.");
        onLoaderDismiss();
      }
    });
  }

  API_Delete() async {
    onLoaderShow();
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        Map _params = {};
        await API.apiCalling(
            url: API.DeleteAPI,
            params: _params,
            method: "DELETE",
            onError: (String errorMessage, bool isError) {
              Utility.alert(errorMessage);
              onLoaderDismiss();
            },
            RESPONSE: (Response) async {
              print("Response");
              print(Response);
              onLoaderDismiss();
            });
      } else {
        Utility.alert("No Internet connection.");
        onLoaderDismiss();
      }
    });
  }

  API_PUT() async {
    onLoaderShow();
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        Map _params = {
          'service_type': '1',
          'venue_type': '3',
          'distance': 50,
          'device_token':
              'cUsb7O6AT-S0Rlt4bag2FQ%3AAPA91bFfKKzGt3kb2Dgwz8ENpOj3KeAh71bPhD_bL-6I_QJWfGQE8xjNthSoSWGYCvwKf_WgVQ7NBv8eqKY6eq5yq9LXbIHYU9-f_OnKEa1VEGka61UuEvZmshHtxYrO8vr8Kyev6PVc',
          'device_type': 'android',
          'app_version': '1.11',
          'timezone': 'asia/kolkata',
          'latitude': '24.1807017',
          'longitude': '72.4021417',
        };

        await API.apiCalling(
            url: API.homesetting,
            params: _params,
            method: "PUT",
            onError: (String errorMessage, bool isError) {
              Utility.alert(errorMessage);
              onLoaderDismiss();
            },
            RESPONSE: (Response) async {
              print("Response");
              print(Response);
              onLoaderDismiss();
            });
      } else {
        Utility.alert("No Internet connection.");
        onLoaderDismiss();
      }
    });
  }
}
